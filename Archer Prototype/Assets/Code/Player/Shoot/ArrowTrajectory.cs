﻿using UnityEngine;
using System.Collections;
using System;

public class ArrowTrajectory : MonoBehaviour {
    public LineRenderer sightLine;
    public ShootProperties properties;
    //Smoothness of a line
    public int segmentCount = 20;
    //Length of each segment
    public float segmentScale = 1;

    private LayerMask lm;
    private int segCount;

    //Used for detecting object we are pointing at
    private Collider hitObject;

    public Collider HitObject
    {
        get { return hitObject; }
    }

    void Start()
    {
        //Setting a layerMask to omit Player layer
        lm = ~(1 << 9);
        segCount = segmentCount;
    }

    void FixedUpdate()
    {
        if (Input.GetButton("Aim") || Input.GetAxis("Aim") == 1)
        {
            SimulatePath();
        }
        else
        {
            sightLine.SetVertexCount(0);
        }
    }


    /// <summary>
    /// Numerical function for calculating approximate trajectory
    /// </summary>
    private void SimulatePath()
    {
        //Used for storing positions of segments
        Vector3[] segments = new Vector3[segCount];
        segments[0] = properties.transform.position;
        //Initial velocity
        Vector3 segVelocity = properties.transform.forward * properties.FireStrength * Time.deltaTime;
        //Reset hitObject
        hitObject = null;

        for (int i = 1; i < segCount; i++)
        {
            //Time it takes to traverse one segment of length segScale
            //t=S/V0
            float segTime = (segVelocity.sqrMagnitude != 0) ? segmentScale / segVelocity.magnitude : 0;

            //Add velocity from gravity for this segment's timestep 
            //(V(t)=Vo + gt
            segVelocity += Physics.gravity * segTime;

            //Checking if we are going to hit anything
            RaycastHit hit;
            if (Physics.Raycast(segments[i - 1], segVelocity, out hit, segmentScale, lm))
            {
                // remember who we hit
                hitObject = hit.collider;
                //Debug.Log("Hit");

                // set next position to the position where we hit the physics object
                segments[i] = segments[i - 1] + segVelocity.normalized * hit.distance;
                // correct ending velocity, since we didn't actually travel an entire segment
                segVelocity = segVelocity - Physics.gravity * (segmentScale - hit.distance) / segVelocity.magnitude;
                segCount = i;
            }
            else
            {
                segments[i] = segments[i - 1] + segVelocity * segTime;
            }
        }

        //Here we apply our calculations to line renderer
        Color startColor = Color.red;
        Color endColor = startColor;
        sightLine.SetColors(startColor, endColor);
        sightLine.SetVertexCount(segCount);
        for(int i = 0; i < segCount; i++)
        {
            sightLine.SetPosition(i, segments[i]);
        }
        segCount = segmentCount;
    }

}
