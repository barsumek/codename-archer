﻿using UnityEngine;
using System.Collections;

public class SimpleCameraFollower : MonoBehaviour
{
    private Transform player;
    private float zdiff = -10;

	void Start()
	{
        player = GameObject.FindGameObjectWithTag("Player").transform;    
	}
	
	void Update()
	{
        float camX = Input.GetAxis("AimX");
        float camY = Input.GetAxis("AimY");
        float aim = Input.GetAxisRaw("Aim");
        Camera.main.transform.position += new Vector3(camX, camY, 0);
        //Camera.main.transform.position = new Vector3(
            //player.position.x, player.position.y, player.position.z + zdiff);
	}
}
