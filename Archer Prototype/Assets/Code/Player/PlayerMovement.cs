﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerMovement : MonoBehaviour {
    public float MovementSpeed = 5f;
    public float jumpForce = 300f;

    private Vector3 movement;
    private float moveX;
    private float distanceToGround;
	

	void Update()
    {
        if (Physics.Raycast(new Ray(transform.position, Vector3.down), 1.3f, 1 << LayerMask.NameToLayer("Ground")))
        {
            if (Input.GetButton("Jump"))
            {
                Jump();
            }
        }
	}

    void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        moveX = Input.GetAxis("Horizontal");
        movement = new Vector3(moveX, 0, 0) * MovementSpeed * Time.deltaTime;
        transform.Translate(movement);
    }

    private void Jump()
    {
        GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce);
    }
}
