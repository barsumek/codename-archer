﻿using UnityEngine;
using System.Collections;

public class ShootProperties : MonoBehaviour {
    private float fireStrength;
    public GameObject Cursor;
    public float cursorMoveSpeed = .5f;
    public float cursorReturnSpeed = 50f;
    public bool drawGizmos = true;
    public float FireStrength
    {
        get { return fireStrength; }
    }
    public float MaxFireStrength=500f;
    private Vector3 lookPos;

	void Update()
    {
        HandleAimingPos();
       //transform.LookAt(lookPos);
	}

    void HandleAimingPos()
    {
        //lookPos = Input.mousePosition;
         // lookPos.z = transform.position.z;
          //lookPos = Camera.main.ScreenToWorldPoint(lookPos);
        float aimY = Input.GetAxis("AimY");
        //transform.rotation = Quaternion.Euler(320+aimY*90,90,0);
        float aimX = Input.GetAxis("AimX");
        fireStrength =500 + Mathf.Sqrt(aimX * aimX + aimY * aimY) * MaxFireStrength;
        //fireStrength =500+aimX * MaxFireStrength;
        
        Vector2 direction = new Vector2(aimX, aimY);

        if (direction != Vector2.zero)
        {
            Cursor.transform.Translate(direction * cursorMoveSpeed * Time.deltaTime);
        }
        else
        {
            if (Vector2.Distance(Cursor.transform.position, Cursor.transform.parent.position) > Mathf.Epsilon)
            {
                Vector2 dir = Vector2.MoveTowards(Cursor.transform.position,
                    Cursor.transform.parent.position,
                    cursorReturnSpeed * Time.deltaTime);
                Cursor.transform.position = dir;
            }
            
        }

    }

    void OnDrawGizmos()
    {
        if (drawGizmos)
        {
            Gizmos.DrawRay(transform.position, Cursor.transform.localPosition);
        }
       
    }
}
