﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {

    bool onAir;

    void Start()
    {
        onAir = true;
    }

	void Update ()
    {
         if (onAir)
        {
            transform.right =
                Vector3.Slerp(transform.right, GetComponent<Rigidbody>().velocity.normalized, Time.deltaTime);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.collider.tag == "Ground")
        {
            onAir = false;
            Debug.Log("Hit arrow");
        }
    }
}
