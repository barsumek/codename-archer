﻿using UnityEngine;
using System.Collections;

public class WeaponHandler : MonoBehaviour
{
    public GameObject weapon;
    public GameObject arrow;
    public ShootProperties properties;
    	
	void Update()
	{
	    if (Input.GetButton("Aim"))
        {
            weapon.SetActive(true);
        }
        else if (Input.GetButtonUp("Aim"))
        {
            var newArrow = (GameObject)Instantiate(arrow, properties.transform.position,ComputeRotation());
            newArrow.GetComponent<Rigidbody>().AddForce(
                properties.transform.forward * properties.FireStrength * Time.deltaTime, ForceMode.Impulse);
            weapon.SetActive(false);
        }
        
	}

    Quaternion ComputeRotation()
    {
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(weapon.transform.position);
        Vector2 mouseOnScreen = (Vector2) Camera.main.ScreenToViewportPoint(Input.mousePosition);
        float angle = AngleBetweenTwoPoints(mouseOnScreen, positionOnScreen);
        Debug.Log(angle);
        Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, angle+80));
        Debug.Log(rotation);
       
        return rotation;
    }

    float AngleBetweenTwoPoints(Vector2 a, Vector2 b)
    {
        
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }
}
